from django.forms import ModelForm
from todos.models import TodoList, TodoItem

# Create a create view for the TodoList model that will show the name field in the form and handle the form submission to create a new TodoList


class TodoListForm(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
