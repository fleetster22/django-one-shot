from django.shortcuts import render, get_object_or_404, get_list_or_404, redirect

from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
# Create a view that will get all of the instances of the
# TodoList model and put them in the context for the template.


def todo_list(request):
    todos = get_list_or_404(TodoList)
    context = {"todo_list": todos}

    return render(request, "todos/todolist.html", context)


def todo_list_detail(request, id=id):
    details = get_object_or_404(TodoList, id=id)
    context = {"todos_details": details}

    return render(request, "todos/todo_list_detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist=form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()

    context= {
        "form": form,
    }
    return render(request, "todos/todo_list_create.html", context)

def todo_list_update(request, id):
    edit_todo = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=edit_todo)
        if form.is_valid():
            edit_todo = form.save()
            return redirect("todo_list_detail", id=edit_todo.id)
    else:
        form = TodoListForm(instance=edit_todo)

    context = {
        "form": form,
    }
    return render(request, "todos/update_todolist.html", context)

def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")

def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", id=todoitem.id)
    else:
        form = TodoItemForm()

    return render(request, "todos/todo_item_create.html", {"form": form})


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm(instance=todoitem)
    return render(request, "todos/update_item.html", {"form": form})
